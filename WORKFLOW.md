# Notre _workflow_

1. Positionnez vous dans votre dépôt (répertoire 1sysd)
2. Crééz un répertoire nommé `TP0`
3. Copiez les fichiers suivant de mon dépôt (à jour) vers le vôtre :
    
   - `Makefile`
   - `bonjour.c`
   - `hello.c`

4. Vérifiez que en tapant `make` les binaires `hello` et `bonjour` sont
   produits, exécutez les

5. Faite en sorte que les trois fichiers cités soient _suivi_ par git

6. Commitez et pushez, vérifez sur gitlab.com qu'ils sont bien là
   bas

Corrigé

_Note_ : Si une commande échoue sur une erreur, c'est généralement
 une *mauvaise* idée de continuer : on lit le message d'erreur,
 on corrige l'erreur et on continue.

~~~~Bash
$ cd ~/1sysd
$ mkdir TP0
$ cd TP0
$ cp ../../1sysdV23a/{Makefile,hello.c,bonjour.c} .
$ ls
Makefile bonjour.c hello.c
$ make
...
$ ls
...
$ ./bonjour
...
$ ./hello
...
$ git add Makefile hello.c bonjour.c
$ git commit -a -m 'TP0'
$ git push
~~~~

_Note_ : Pensez, si vous avez fait l'exercise sur le système montpellier, à faire
un git pull à partir du répertoire 1sysd de votre système Linux !
