# 1SYSD, classe 1B1 Versailles, Examen (4h)

_N.B._ Le travail demandé est attendu dans votre dépôt GIT dans
un répertoire nommé `Examen` ne contenant pas de binaires. Le fichier
_Makefile_ doit faire en sorte que la commande `make` produise
tous les binaires exécutables. 

## A. Fahrenheit et Celsius

_Fichier à fournir :_ `tconvert.c`

Il existe deux unités de mesure de température communément utilisées :
le degré Celsius °C (d'usage dans une majorité de pays) et le degré
Fahrenheit °C d'usage en particulier aux E.-U. d'Amérique.

Si _C_ est une température mesurée en degrés Celius, la température
correspondante en degrés Fahrenheit est donnée par la formule : 

F = (9/5) * C + 32

La formule de conversion inverse est donc :

C = (F - 32)*5/9

Écrire en C les deux fonctions de conversion : `celsius2fahrenheit` et
`fahrenheit2celsius` qui acceptent un argument de type `float`et renvoient
une valeur du même type.

Dans le programme principal demander à l'utilisateur s'il souhaite
convertir de °C à °F (saisie du caractère `1`) ou l'inverse 
(saisie du caractère `2`), puis demande la saisie d'un nombre et
affiche le résultat de la conversion.

## B. Compter des caractères

_Fichiers à fournir :_ `countchar1.c` et `countchar2.c`

Vous pouvez retrouver dans `TP3/showargv.c` comment on peut lire les
arguments passés en ligne de commande.

1. Écrire une fonction `count_char` qui accepte deux arguments : une
chaîne de caractère et un caractère et renvoie combien de fois
le caractère est présent dans la chaîne.

Dans le programme principal traiter le premier argument reçu
comme le caractère à dénombrer et le second comme la chaîne
à examiner. Exemple d'usage :

~~~~Bash
$ ./countchar1 "to be or not to be" "o"
4
$ ./countchar1 "to be or not to be" "O"
0
$ ./countchar1 "to be or not to be" "z"
0
~~~~

2. Ajouter un argument optionnel `-i` en dernière position qui rend le
comptage insensible à la casse (i.e. compter 'a' ou 'A' indifféremment
si le caractère à dénomber est 'a' ou 'A'). Exemple d'usage :

~~~~Bash
$ ./countchar2 "to be or not to be" "o"
4
$ ./countchar2 "to be or not tO be" "O"
1
$ ./countchar2 "to be or not tO be" "o" -i
4
$ ./countchar2 "to be or not tO be" "O" -i
4
$ ./countchar2 "to be or not to be" "z" -i
0
~~~~

## C. Compter des nombres

_Fichier à fournir :_ `countint.c`

Écrire une fonction `compte_int`qui accepte trois arguments : un
tableau de valeurs de type `int`, la taille du tableau et une valeur.
La fonction renvoie le nombre de fois où l'on trouve la valeur
dans le tableau.

Dans le programme principal tester le bon fonctionnement de
la fonction ainsi :

~~~~C
int main() {
    int t1[5] = { 2, 42, 1, 42, 9 };
    int t2[10] = { 1, 2, 3, 4, 5, 6, 7, 8, 9, 10 };


    printf("%d dans t1 : %d fois.\n", 42, compte_int(t1, 5, 42));
    printf("%d dans t1 : %d fois.\n",  2, compte_int(t1, 5,  2));
    printf("%d dans t2 : %d fois.\n", 10, compte_int(t2, 10, 10));
    printf("%d dans t2 : %d fois.\n", 42, compte_int(t2, 10, 42));
}
~~~~

## D. Listes chaînées

Prenez le temps de lire et d'exécuter le programme dont le source
est `intlist.c`.

1. Ajoutez et testez une fonction `int total(Node *head)` qui calcule
 et renvoie la somme des valeurs d'une liste d'entiers.

2. Ajoutez et testez une fonction `float average(Node *head)` qui calcule
 et renvoie la moyenne d'une liste d'entiers (non vide).

3. Ajoutez et testez une fonction `Node *insert_value(Node *head, int value)`
 qui insère une valeur en début de liste.

