# Préparation de la connexion avec ssh à notre instance dans le Cloud

Rappel : La cryptographie à clef publique repose sur la création de deux
clefs l'une dite _privée_ et l'autre dite _publique_ :

- Privé signifie ici : à ne communiquer à _personne_ !!!
- Publique signifie ici : il n'y a aucun problème, au contraire, à la partager

SSH _(Secure Shell)_ permet de se connecter à distance à un
système (le plus souvent UNIX ou GNU/Linux), le serveur
transmet sa clef publique au serveur (ce qui ne pose pas
de problème, sinon d'authenticité : d'où l'avertissement
que vous verrez lors d'une première connexion).

Il est ensuite possible pour un utilisateur de créer des
paires de clef publique/privée personnelle et d'utiliser
la clef privée pour prouver au serveur SSH qu'il est bien
qui il prétend être (sans donner de mot de passe) mais,
pour cela, il faut que la clef publique soit dans une
liste de confiance du côté du serveur : c'est le 
fichier `~/.ssh/authorized_keys`.

Si vous pouvez vous connecter avec mot de passe vous
pouvez insérer votre clef _publique_ dans cette liste.

Dans une machine virtuelle dans un Cloud IaaS il est
généralement interdit de se connecter par mot de passe
au départ (et c'est mieux de rester ainsi, sauf pour
une courte période de temps, pour transférer les clefs).

_Note_ : Sous UNIX ou GNU/Linux on peut utiliser le
script `ssh-copy-id` pour faire ce transfert, si le
client SSH est sous Windows on peut le faire à la 
main. Ici je vais le faire (je suis admin de la VM
sur AWS) pour vous, pour chaque compte utilisateur.

## Création d'une paire de clés privée/publique pour SSH

Note : Répondre [Entrée] à toutes les questions.

~~~~Bash 
$ ssh-keygen -t rsa 
Generating public/private rsa key pair.
Enter file in which to save the key (/home/jpierre/.ssh/id_rsa): 
Created directory '/home/jpierre/.ssh'.
Enter passphrase (empty for no passphrase): 
Enter same passphrase again: 
Your identification has been saved in /home/jpierre/.ssh/id_rsa
Your public key has been saved in /home/jpierre/.ssh/id_rsa.pub

Generating public/private rsa key pair.
Enter file in which to save the key (/home/jpierre/.ssh/id_rsa): 
Created directory '/home/jpierre/.ssh'.
Enter passphrase (empty for no passphrase): 
Enter same passphrase again: 
Your identification has been saved in /home/jpierre/.ssh/id_rsa
Your public key has been saved in /home/jpierre/.ssh/id_rsa.pub
...
$ ls -l .ssh/
total 8
-rw------- 1 jpierre jpierre 2610 Jun 27 08:04 id_rsa
-rw-r--r-- 1 jpierre jpierre  576 Jun 27 08:04 id_rsa.pub
~~~~

Notez que les droits sur `id_rsa` sont tels que seul vous pouvez lire
son contenu.

Afficher son contenu, sélectionnez-le et copiez le dans le presse papier.

~~~~Bash
$ cat .ssh/id_rsa.pub 
ssh-rsa AAAAB3NzaC1yc2EAAAADAQABAAABgQDkcNLDtu+pm/Z+lq8R13WWGXKgrgnYBcmJTnIXvNyVSwPcIPECmK5HpvK6JZjTcw+pdr8y
...
... FDsid4J7iSrx6lqdW+bF05nyH3+wz2/yLBtkfGnHFQY58= jpierre@ip-172-31-21-3
~~~~ 

Postez son contenu dans le canal du cours.

Note : de mon côté qui suis admin de la machine sur AWS j'ai ajouté ce contenu
dans le fichier `~/.ssh/authorized_keys` (pour chacun d'entre vous).

À partir de là, votre client ssh peut prouver au serveur ssh de la VM sur AWS
que vous êtes bien qui vous prétendez être.

Vous pouvez réitérer la création d'une paire de clés autant que vous voulez
(y compris sous MS Windows) et ajouter la clef publique à la suite dans
ce même fichier. Et ainsi accéder à la VM à partir d'un autre poste
client.

## Partager une session de terminal avec moi

Une fois connecté en SSH à la VM dans AWS, vous pourrez partager mon 
terminal en faisant simplement `tmux -S /tmp/hexa attach -t hexa`.

Nous allons éditer un fichier texte nommé `.bashrc` pour que soit
encore plus simple : `nano ~/.bashrc` et y mettre la ligne :

~~~~
alias jpterm='tmux -S /tmp/hexa attach -t hexa -r'
~~~~

Testez que ça fonctionne :

~~~~Bash
$ exit
$ exit
(jusqu'à ne plus avoir 'montpellier' dans l'invite du Shell)
$ ssh votre_prenom@ip_de_la_vm
$ jpterm
~~~~

Quand vous êtes en partage de ma session de terminal pour
quitter la séquence de touche est CTRL+B puis d (comme
_detach_).

Mon `.bashrc`, lui, définit trois alias :

~~~~Bash
alias jpterm='[ -w /tmp/hexa ] && tmux -S /tmp/hexa attach -t hexa -r || echo "No writable /tmp/hexa"'
alias mkterm='tmux -S /tmp/hexa new -s hexa'
alias startshare='chgrp hexa /tmp/hexa'
~~~~

## Création d'une autre paire de clefs sur la VM montpellier sur AWS.

De la même façon vous pouvez créer une paire de clef publique/privée
sur montpellier :

~~~~Bash
$ ssh-keygen -t rsa
$ cat ~/.ssh/id_rsa.pub
~~~~

Que vous pourrez ajouter plus tard à votre profile gitlab.

