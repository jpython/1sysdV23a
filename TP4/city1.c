#include<stdio.h>
#include<stdlib.h>

typedef struct City City;
struct City {
    char *name; // ou pourrait envisager char name[50]
    int pop;
    float area;
};

void print_city(City city) {
    printf("Ville de %s\n", city.name);
    printf("  Population : %d habitants\n", city.pop); 
    printf("  Surface : %.2f km²\n", city.area); 
}

City read_city() {
    City city;

    printf("Nom de la ville (pas d'espace dans les nom) : ");
    scanf("%ms", &city.name);
    printf("Population : ");
    scanf("%d", &city.pop);
    printf("Surface : ");
    scanf("%f", &city.area);

    return city;
}


int main() {
    City city;

    city = read_city();
    print_city(city);

    exit(0);
}

