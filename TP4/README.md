# Structures en C

## Structures et tableaux de structures

En partant de `city1.c` écrire une fonction qui affiche les
information sur une ville : `void print_city(City c)`

Écrire une fonction `City read_city()` qui interroge l'utilisateur
en lui demandant les informations et renvoie une structure de
type City.

Testez.

Copier `city1.c` en `city2.c`

Ajoutez y la demande à l'utilisateur d'un nombre
de ville, puis la saisie des informations sur ce nombre
de villes et enregistre les info dans tableau.

On se contentera d'allouer dans `main` un tableau de
ville de taille maximale 50 :

~~~~C

City cities[50];

...

for ( i = 0 ; i < N ; i++) {
    print_city(cities[i]);
}
~~~~

