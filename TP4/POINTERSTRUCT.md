# Pointeurs vers structures

## Syntaxe 

~~~~C

City *p;

...

printf("%s", p->name); // plus lisible (*p).name
~~~~

## À quoi ça peut bien servir ?

Imaginons qu'on souhaite stocké une liste arbritrairement longue
de villes... Allouer un tableau de plusieurs centaines de milliers
d'éléments n'est pas raisonnable...

On souhaite travailler dynamique. Par exemple si j'ajoute une ville
à mon jeu de données la mémoire est allouée à ce moment là.

Un représentation possible est une liste chaînée : chaque éléments
va avoir un champ en plus, typiquement nommé `next` (ou `suivant`)

~~~~C
typedef struct City city;
struc City {
    char name[50];
    int pop;
    float area;
    City *next; // pointeur vers la ville suivante
                // NULL si c'est la dernière
}
~~~~

~~~~
        | name: Paris         ----> | name: Montpellier       --->| name: Versailles
head -> | pop: 20000000      /      | pop: 800000            /     | pop: 500000
        | area: 71.23       /       | area: 100.12          /      | area: 20.61
        | next: -----------/        | next: ---------------/       | next : NULL
~~~~

Pour créer une ville :

~~~~C

City *read_city() {
    City *city;

    city = malloc(sizeof(City));
    ... saisie des données
    city->next = NULL;
    return city;
}

~~~~
