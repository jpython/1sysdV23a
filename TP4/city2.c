#include<stdio.h>
#include<stdlib.h>

typedef struct City City;
struct City {
    char *name; // ou pourrait envisager char name[50]
    int pop;
    float area;
};

void print_city(City city) {
    printf("Ville de %s\n", city.name);
    printf("  Population : %d habitants\n", city.pop); 
    printf("  Surface : %.2f km²\n", city.area); 
}

City read_city() {
    City city;

    printf("Nom de la ville (pas d'espace dans les noms) : ");
    scanf("%ms", &city.name);
    printf("Population : ");
    scanf("%d", &city.pop);
    printf("Surface : ");
    scanf("%f", &city.area);

    return city;
}


int main() {
    City cities[50];
    int N;

    printf("Nombre de villes : ");
    scanf("%d", &N);

    for ( int i = 0 ; i < N ; i++) {
        cities[i] = read_city();
    }

    for ( int i = 0 ; i < N ; i++) {
        print_city(cities[i]);
    }
    exit(0);
}

