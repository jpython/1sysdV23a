#include<stdio.h>
#include<stdlib.h>

int main() {
	int tab[] = { 2, 12, 42, 13 };
	int i;

        long int prec;
	prec = (long int)&tab[0];
	for ( i = 0; i < 4; i++) {
		printf("Élément %d du tableau %d\n", i, tab[i]);
		printf("Élément %d du tableau %d\n", i, i[tab]);
		printf("Adresse de l'élément %lu\n" , &tab[i]);
		// cast : (type souhaité)expression...
		printf("Différence avec élément précédent : %lu\n",
				(long int)&tab[i] - (long int)prec);
		prec = (long int)&tab[i];
	}
	
	int *p; // pointeur vers un entier
	p = tab;
	while ( *p != 13 ) {
		printf("Élement courant %d\n", *p);
		p++;
	}
	// ci-dessus c'est un peu absurde, parce que le dernier
	// élément du tableau n'est pas nécessairement 13, et en
	// plus du coup on le rate
	//
	// En revanche pour les chaînes de caractère c'est parfait :
	// Le marqueur de fin est justement une valeur : 0

	char text[] = "Bonjour !";
	char *pp;
	pp = text;
	while ( *pp ) {
		printf("Caractère : %c\n", *pp);
		pp++;
	}

	p = (int *)42; // pourquoi pas ?
	printf("L'entier situé à l'adresse 42 vaut :\n");
	printf("%d\n", *p);

	exit(0);
}
