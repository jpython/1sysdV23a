# Exercice : tableau, caractère et chaînes de caractères

En partant de texte.c et Makefile (à copier dans un repertoire
de votre dépôt GIT, add + commit + push)

~~~~Bash
$ cd ~/1sysdV23a
$ git pull

$ cd ~/1sysd
$ mkdir TP3
$ cd TP3
$ cp ../../1sysdV23a/TP3/* .
$ make
$ ./texte
...
$ git add texte.c Makefile README.md
$ git commit -a -m 'start'
$ git push
$ nano texte.c 
~~~~

(NB : vous pouvez utiliser l'éditeur de texte nano, ou vim, ou
 gedit, ou ce que vous voulez !)

- Ajoutez du code afin de compter le nombre d'espaces dans la
  chaîne saisie et l'afficher à la fin.

- Quelle est la différence entre le code ASCII de 'A' et de 'a' ?
  De 'B' et de 'b', de 'M' et de 'm' ? Que concluez-vous ?

- Ajoutez du code qui modifie la chaîne `texte` et passe tous
  les caractères en majuscule s'il sont en minuscule et 
  affiche la nouvelle chaîne. NB : sans utiliser de fonction
  de la libc (pas de `string.h`)

- Ajoutez du code pour compter le nombre de mots dans la phrase
  saisie en supposant qu'ils sont séparés par exactement UNE
  espace.

À partir d'ici le but est de compter le nombre de _mots_ :

- Modifiez le code pour que le comptage du nombre de mots soit
  correct si une ou plusieurs espaces séparent certains mots.

- Votre code fonctionne-t-il correctement si vous ajoutez des
  espace au début ou la fin de la chaîne ? Et avec une chaîne vide ?
  Avec des mots d'une seule lettre ? Avec un peu tout ça à
  fois ? (corrigé : `compte.c`)

- Copiez le fichier source sous un autre nom ; Réécrivez les
  boucle while sous forme de boucle for 

- Copiez le fichier encore sous un autre nom ; remplacer les
  expressions de la forme `texte[i]` par `i[texte]` ; qu'est-ce
  que ça donne ?
 
