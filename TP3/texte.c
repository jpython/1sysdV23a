#include<stdio.h>
#include<stdlib.h>

int main() {
    char texte[100]; // chaîne de taille <= 999 (zéro terminal !)
    int i, nesp; 

    printf("Tapez une phrase : "); 
    scanf("%[^\n]", texte);
    printf("Phrase : %s\n", texte);

    i = 0;
    while (texte[i] != '\0') { // ou != 0
       printf("%c ", texte[i]);
       i++;
    }
    printf("\nLongueur de la chaîne : %d\n", i);

    // Compter le nombre d'espace(s)
    i = 0;
    nesp = 0;
    while (texte[i] != 0) {
        if ( texte[i] == ' ' ) {
	    nesp++;
	}
        i++;
    }
    printf("Nombre d'espace(s) : %d\n", nesp);

    // En python :
    // >>> ord('m') - ord('M') 
    // 32

    printf("%d %d %d\n", 'm' - 'M', 'a' - 'A', 'b' - 'B');

    // minuscule -> majuscule :  code ascii - 32

    i = 0;
    while (texte[i] != '\0') {
        if ( texte[i] >= 'a' && texte[i] <= 'z' ) { // minuscule
            // texte[i] -= 32; 
	    // - 32 c'est pareil que & 0b1011111
	    texte[i] &= 0b1011111;
	}
	i++;
    }
    printf("Nouvelle chaîne : %s\n", texte);

    exit(0);
}
