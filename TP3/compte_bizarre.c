#include<stdio.h>
#include<stdlib.h>

// on reprend compte.c et on remplace TOUS
// les texte[i] par i[texte] (ça paraît absurde, non ?)
// ça compile... quoi ?
// ça marche ! quoi !!!!!?


int main() {
    char texte[100]; // chaîne de taille <= 999 (zéro terminal !)
    int i, nmots; 

    printf("Tapez une phrase : "); 
    scanf("%[^\n]", texte);
    printf("Phrase : %s\n", texte);

    i = 0;
    nmots = 0;
    // On commence par avancer jusqu'au premier caractère non espace
    // (jusqu'au début du premier mot)
    while (i[texte] == ' ') {
	    i++;
    }
    // Arrivé ici on sait qu'on est sur le premier caractère du
    // premier mot (ou à la fin de la chaîne (i.e. 0) si chaîne
    // vide (ça tombe bien car nmot est à 0 initialement)
    while (i[texte] != '\0' ) { /// surtout pas '0' qui est 48 !
	// on a trouvé un début de mot : le compteur augmente
	nmots++;
	// on va jusqu'à la fin du mot :
	// espace ou fin de la phrase
	while (i[texte] != ' ' && i[texte] != '\0' ) {
		    i++;
	}
	// arrivé ici on est soit sur une espace
	// soit à la fin de la chaîne
	while (i[texte] == ' ') {
		i++;
	}
    }
    printf("Nombre de mots(s) : %d\n", nmots);

    exit(0);
}
