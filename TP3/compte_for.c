#include<stdio.h>
#include<stdlib.h>

int main() {
    char texte[100]; // chaîne de taille <= 999 (zéro terminal !)
    int i, nmots; 

    printf("Tapez une phrase : "); 
    scanf("%[^\n]", texte);
    printf("Phrase : %s\n", texte);

    // avec le recule : *je* trouve que c'était plus lisible
    // avec des while qu'avec des for

    // On commence par avancer jusqu'au premier caractère non espace
    // (jusqu'au début du premier mot)
    //  Note boucle for sans corps
    for ( i = 0; texte[i] == ' '; i++);

    // Arrivé ici on sait qu'on est sur le premier caractère du
    // premier mot (ou à la fin de la chaîne (i.e. 0) si chaîne
    // vide (ça tombe bien car nmot est à 0 initialement)
    // Note : partie incrémentation vide !
    for ( nmots = 0; texte[i] != '\0';  ) { /// surtout pas '0' qui est 48 !
	// on a trouvé un début de mot : le compteur augmente
	// et on va jusqu'à la fin du mot :
	// espace ou fin de la phrase
	for ( nmots++; texte[i] != ' ' && texte[i] != '\0'; i++ );
	// arrivé ici on est soit sur une espace
	// soit à la fin de la chaîne
	for ( ; texte[i] == ' '; i++);
    }
    printf("Nombre de mots(s) : %d\n", nmots);

    exit(0);
}
