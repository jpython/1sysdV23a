#include <stdio.h>
#include <stdlib.h>

void print_binary(unsigned long int number) {
    if (number >> 1) {
        print_binary(number >> 1);
    }
    putc((number & 1) ? '1' : '0', stdout);
}

int main() {
    double pi = 3.1415926;
    double minuspi = -3.1415926;
    double quasipi = 3.1416;

    print_binary( *(  (unsigned long *)&pi));
    printf("\n");
    print_binary( *(  (unsigned long *)&minuspi));
    printf("\n");
    print_binary( *(  (unsigned long *)&quasipi));
    printf("\n");
    exit(0);
}
