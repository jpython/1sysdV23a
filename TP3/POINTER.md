## Pointeurs (pas de panique)

- Un pointeur c'est un nombre correspondant à une adresse 
  en mémoire
- On indique le type de la données supposée stockée à cette
  adresse ce qui permet de connaître sa taille (combien d'octets)
  lors de la compilation
- La connaissance du type permet aussi de faire correctement les
  opérations sur ces données
- Les opération sur les pointeurs sont possibles (arithmétique
  sur pointeurs) :
    - p + n : n cases plus loin que p
    - p - n : n cases en arrière avant p
    - par extention : p++ (p = p + 1) et p-- (p = p - 1)
    - p2 - p1 : combien d'éléments (dans un tableau) entre
      l'adresse p1 et l'adresse p2

On comprend mieux pourquoi `tab[i]` et `i[tab]` c'est pareil,
en réalité c'est `*(tab + i)` : valeur présente à l'adresse
tab + i fois la taille d'un case du tableau.

## Notation :

Déclaration : `type *nom_de_variable`, ex : `int *p` : déclare un
pointeur vers un entier, `char *p` : déclarer un pointeur vers un
caractère.

Usage : si déclaré comme `int *p` et `int n`

- `p` : la valeur de l'adresse mémoire
- `*p` : la valeur de type `int` présente à cette adresse ; on parle 
   de _déréférencement_ (on suit la référence - l'adresse - qu'est p pour
   atteindre la valeur qui y est stocké)
 (note : * a une faire priorité : `*p++` est équivalent à *(p++) qui
  a la même valeur que `*p` - p est augmenté après évaluation, 
  `(*p)++` : on regarde la valeur stocké à l'adresse p et on l'augmente de p.
- `&n` : l'adresse mémoire ou `n` est stocké (pointeur vers int, type : `int *`)


~~~~C
#include...

int main() {
    int *p, n = 42;

    p = &n;
    printf("%d\n", *p); // 42
    printf("%d\n", n);  // 42
    printf("%d\n", *(&n)); // 42
    printf("%lx\n", (unsigned long int)&p); // ???
    printf("%lx\n", (unsigned long int)&n); // ??? (différent du précédent)
    printf("%lx\n", (unsigned long int)&(*p)); // ??? (idem précédent)
    exit(0);
}
~~~~

## Exercices : manipulation de chaînes en utilisant des pointeurs

- Écrire une fonction qui renvoie la longueur d'une chaîne passée en argument
- Écrire une fonction `is_upper` qui renvoie 1 si les caractères alphabétiques
  (ASCII) de la chaîne reçue en arguments sont tous majuscules et 0 sinon
- Écrire une fonction qui reçoit deux chaînes de caractères en argument et
  renvoie 1 si elle sont égales et 0 si elles sont différentes
- Réécrire le programme qui compte les mots en utilisant des pointeurs
  vers char au lieu d'indices numériques
- Écrire une fonction qui transforme une chaîne de caractère en remplaçant
  les caractères alphabétiques minuscules en majuscules

_Note_ (philosophique) : Une chaîne vide est-elle en capitales ? Je dis que oui.
Elle est aussi entièrement en minuscule.

Concernant l'égalité de deux chaîne :

- Il faut parcourir les chaînes UNE SEULE FOIS
- On ne connaît pas les longueurs des chaînes !
- On parcourt simultanément les deux chaînes avec
  deux pointeurs p et q qui avancent ensemble

En cours de traitement :
~~~~
  s1 : Bonjour0
         ^
         p
  s2 : Bonjour comment ça va ?0
         ^
         q 
~~~~

Si les chaînes sont différentes parce que l'une
est finie, et pas l'autre (faut renvoyer 0):

~~~~
  s1 : Bonjour0
              ^
              p
  s2 : Bonjour comment ça va ?0
              ^
              q 
~~~~

Si on tombe sur deux caractère différents, on renvoie 0.

~~~~
  s1 : Bonjour0
          ^
          p
  s2 : Bonne nuit0
          ^
          q
~~~~

Si on arrive en même temps à la fin, on renvoie 1

~~~~
  s1 : Bonjour0
              ^
              p
  s2 : Bonjour0
              ^
              q
~~~~

À chaque itération on regarde si *p et *q sont égaux, si oui
 et qu'aucun des deux n'est 0 (ou '\0', c'est pareil) il faut continuer...

Quand sortir de la boucle ?
- *p n'est pas égal à *q ; réponse : 0
- Si l'on est arrivé à la fin d'une des chaînes et pas de l'autre ; réponse : 0 
- Si les deux sont terminées ; réponse : 1

## Pour terminer...

En prenant `showargs.c` comme modèle, modifier la fonction `main` de
`strings_exos_args.c` pour qu'il ne demande plus de chaîne à l'utilisateur
mais s'attends à les lire dans `argv` (éléments d'indices 1 et 2).
Pensez à vérifier que argc vaut bien 3 et si il a une autre valeur quitter
avec code de retour non nul (`exit(1)`). 






