#include<stdio.h>
#include<stdlib.h>

int slength(char s[]) {
    int l = 0;
    char *p;
    p = s;
    while ( *p++ ) { // tant qu'on est pas sur le zéro final...
        l++;         // on augmente l
    }
    return l;
}

int is_upper(char s[]) {
    char *p;
    int up = 1;
    for ( p = s ; *p && up ; p++ ) {
        if ( *p >= 'a' && *p <= 'z' ) {
            up = 0;
        }
    }
    return up;
}

int is_upper2(char s[]) {
    char *p;
    int up = 1;
    p = s;
    while (*p && up) {
        if ( *p >= 'a' && *p <= 'z' ) {
            up = 0;
        }
        p++;
    }
    return up;
}

int sequal(char s1[], char s2[]) {
    char *p, *q;
    p = s1;
    q = s2;
    while ( *p && *q && (*p == *q) ) {
        p++;
        q++;
    }
    // Ici on SAIT que la condition *p && *q && (*p == *q) est fausse
    // au choix, ça revient au même
    // si p et q ont épuisé les deux chaine : VRAO
    // dans ce cas *p et *q sont égaux (et valent 0)
    // return *p == '\0' && *q == '\0';
    return *p == *q;
}

int nb_words(char s[]) {
    char *p;
    int nmots = 0;
    // On commence par avancer jusqu'au premier caractère non espace
    // (jusqu'au début du premier mot)
    p = s;
    while ( *p == ' ' ) {
        p++;
    }
    // Arrivé ici on sait qu'on est sur le premier caractère du
    // premier mot (ou à la fin de la chaîne (i.e. 0) si chaîne
    // vide (ça tombe bien car nmot est à 0 initialement)
    while ( *p != '\0' ) { /// surtout pas '0' qui est 48 !
	    // on a trouvé un début de mot : le compteur augmente
	    nmots++;
	    // on va jusqu'à la fin du mot :
	    // espace ou fin de la phrase
	    while ( *p != ' ' && *p != '\0' ) {
		    p++;
	    }
	    // arrivé ici on est soit sur une espace
	    // soit à la fin de la chaîne
	    while ( *p == ' ' ) {
            p++; 
        }
    }
    return nmots;
}

// TODO: lire les chaînes dans les arguments reçus (argv)
int main(int argc, char *argv[]) {
    // pas de taille max car on utilise GNU scanf... 
    // cf. plus bas et man scanf
    char *texte, *texte2; 

    if ( argc != 3 ) { 
        printf("Usage : %s \"phrase1i\" \"phrase1\" \"phrase2\"\n", argv[0]);
        exit(1);
    }

    texte = argv[1];
    texte2 = argv[2];

    printf("Longueur de la première chaîne : %d\n", slength(texte));
    if (is_upper2(texte)) {
        printf("La première chaîne est entièrement en capitales.\n");
    } else {
        printf("La chaîne n'est pas entièrement en capitales.\n");
    }
    if (sequal(texte, texte2)) {
        printf("Les deux chaînes sont identiques.\n");
    } else {
        printf("Les deux chaînes sont différentes.\n");
    }
    printf("La première chaîne contient %d mot(s).\n", nb_words(texte));
    exit(0);
}
