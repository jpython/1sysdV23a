#include<stdio.h>

// comment lire un caractère unique ?

int main() {
	char c;

	printf("Tapez un caractère puis entrée : ");
	// notez l'espace avant le %c
	scanf(" %c", &c);
	printf("C'est : %c\n", c);
	if ( c == 'a' ) { // apostrophe
	    printf("C'est un 'a' !\n");
	} else {
	    printf("Ce n'est pas un 'a' !\n");
	}

	return 0;
}
