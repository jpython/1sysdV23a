#include<stdio.h>
#include<string.h>

int main() {
    char text1[] = "Hello";
    char text2[] = { 'H', 'e', 'l', 'l', 'o', 0 }; // '\0' aussi, '0' non !!
    char text3[] = { 72, 101, 108, 108, 111, 0 };

    /* lu : unsigned long */
    printf("%s %lu\n", text1, strlen(text1));
    printf("%s %lu\n", text2, strlen(text2));
    printf("%s %lu\n", text3, strlen(text3));

    if (strcmp(text1, text2) == 0) {
        printf("text1 est égal à text2 pour strcmp\n");
    }    
    if (strcmp(text1, text3) == 0) {
        printf("text1 est égal à text3 pour strcmp\n");
    }    

}
