
#include<stdio.h>
#include<stdlib.h>
#include<unistd.h>

int main() {
	int pid;

	if (pid = fork()) {
		// pid > 0 : on est le parent
		printf("Processus %d lancé...\n", pid);
		sleep(10);
	} else {
		// pid = 0 : on est le fils
		printf("Processus fils lancé...\n");
		sleep(2);
		printf("Processus fils se termine...\n");
		exit(EXIT_SUCCESS);
	}
	printf("Fin du processus parent...\n");
	exit(EXIT_SUCCESS);
}

