#include<stdio.h>
#include<stdlib.h>

typedef struct node node;
struct node {
   int val;
   node *next;
};

node *create_node(int val) {
    node *p;
    p = malloc(sizeof(node));
    p->val = val;
    p->next = NULL;
    return p;
}

void print_list(node *head) {
    node *walk;

    walk = head;
    while (walk != NULL) { // ou juste walk 
        printf("%d ", walk->val);
        walk = walk->next;
    }
    printf("\n");
}

node *append_val(node *head, int val) {
    node *newnode, *walk;

    newnode = create_node(val);
   
    // note : on peut omettre tous les "!= NULL"
    // un pointeur est "faux" ssi il est NULL
    if (head == NULL) { // liste vide
        head = newnode;
    } else {            // on parcourt la liste jusqu'à la fin
        walk = head;
        while (walk->next != NULL) { // on va jusqu'au dernier nœud
            walk = walk->next;
        }
        walk->next = newnode; // on ajoute le nouvel élément
    }
    return head;
}

void *append_val2(node **phead, int val) {
    node *newnode, **walk;

    newnode = create_node(val);
    
    walk = phead;
    while (*walk) {
	    walk = &( (*walk)->next );
    }
    *walk = newnode;
}

int main() {
    node *head1 = NULL, *head2 = NULL;

    head1 = append_val(head1, 42);
    head1 = append_val(head1, 12);
    head1 = append_val(head1, -5);
    head1 = append_val(head1, 41);
    
    append_val2(&head2, 42);
    append_val2(&head2, 12);
    append_val2(&head2, -5);
    append_val2(&head2, 41);

    print_list(head1);
    print_list(head2);

}
