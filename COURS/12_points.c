#include<stdio.h>
#include<stdlib.h>

typedef struct Point Point
struct Point {
	char label[10];
	long double x,y;
};

int main() {
	Point p1 = { "Maison", 12.1, 56.7 };
	Point p2 = { "Bureau", 1.9, 6.7 };
	Point p3;
	Point ligne[10];

	p3.label = "Bar";
	p3.x = 6.5;
	p3.y = 3.12;

        printf("%s(%.2f,%.2f)", p1.label, p1.x, p2.x);
	
	return 0;
}

