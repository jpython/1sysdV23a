#include<stdio.h>
#include<stdlib.h>
#include<string.h>

void cesar(char *message, int key, char *crypted) {
	char *p, *q, c, e, start;

	p = message;
	q = crypted;
	while (c = *p++) { // pas un test ! Simple = : affectation
			   // et faux si 0 comme d'habitude
		start = 0;
		if ( c >= 'a' && c <= 'z' ) {
			start = 'a';
		} else if ( c >= 'A' && c <= 'Z') {
			start = 'A';
		}
		if (start) {
			e = (c - start + key) % 26 + start;
		} else {
			e = c;
		}
	    	*q++ = e;
	}
	*q = '\0';
}

void usage(char *prog) {
	printf("Usage: %s [-d] key"
		"\n\n  Encrypt standard input using Cesar's algorithm"	       
		"\n  -d: decrypt instead of encrypt"
		" (0 < key < 26)\n\n", prog);
	exit(EXIT_FAILURE);
}

int main(int argc, char *argv[]) {
	
        char *line = NULL;
        size_t len = 0;
        ssize_t nread;

	int key, decrypt; 
	char *crypted;
	// si plus compliqué que ça : bibliothèque getopts
	switch(argc) {
		case 1: // pas d'argument reçu
			usage(argv[0]);
		case 2: // un argument reçu (clef ou -h)
			if (strcmp(argv[1], "-h") == 0) {
				usage(argv[0]);
			}
			decrypt = 0;
			key = strtol(argv[1], NULL, 10);
			break;
		case 3: // deux arguments reçus (-d et clef)
			if (strcmp(argv[1], "-d") == 0) {
				decrypt = 1;
				key = strtol(argv[2], NULL, 10);
			} else {
				usage(argv[0]);
			}
			break;
		default:
			usage(argv[0]);
	}
	if (key < 1 || key > 25) {
		usage(argv[0]);
	}

	// arrivé ici : key est la clef, et decrypt vaut 0 pour chiffrer
	// et 1 si c'est pour déchiffrer
	
	// distinguer chiffer/déchiffrer
	if (decrypt) {
		key = -key;
	}
	// man getline
	while ((nread = getline(&line, &len, stdin)) != -1) {
		crypted = malloc((nread + 1)*sizeof(char));
        	cesar(line, key, crypted);
		fwrite(crypted, nread, 1, stdout);
		free(line);
		free(crypted);
		line = NULL;
	}
	exit(EXIT_SUCCESS);
}

