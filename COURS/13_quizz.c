#include<stdio.h>
#include<stdlib.h>

int main() {
    long int n = 42;
    char msg[] = "Hello";
    long int *p1, *p2;
    char *p3;
    // État de la mémoire ici ?
    p1 = (long int *)n;
    p2 = &n;

    (*p2)++;

    p3 = msg;
    *p3 = 'B';
    p3++;
    // etc ici...
    printf("n *p1 *p2 = %d %d %d\n", n, *p1, *p2);
    printf("msg *p3 = %s %c\n", msg, *p3);

} 
