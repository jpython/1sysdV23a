#include<stdio.h>

int main() {
    int n, i, s;

    printf("Merci de saisir 5 nombres entiers\n");
    s = 0;
    for (i = 0; i < 5; i++) {
        printf("nombre n°%d : ", i + 1);
        scanf("%d", &n);
        s += n;
    }
    printf("Somme : %d\n", s);
}
