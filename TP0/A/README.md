# Cours 1SYSD : révisions

## Tableaux d'entiers

1. Écrire une fonction `fibotab(int n)` qui renvoie le tableau des termes
de la suite de Fibonnaci de 0 à n - 1. Tester la en écrivant un programme
qui fonctionne ainsi :

~~~~Bash
$ ./fibotab 7
1 1 2 3 5 8 13
$ 
~~~~

2. Écrire une fonction `sieve(int n)` qui implémente le crible d'Ératosthène
pour renvoyer un tableau de n entiers. Partez d'un tableau contenant 2, puis
tous les nombres impairs jusqu'à n (2, 3, 5, 7, 9, 11, 13, 15, ...). 
L'algorithme dit du _crible d'Érathostène_ permet de facilement remplacer
par 0 les éléments du tableau qui ne sont pas premiers.

Testez en affichant les termes non nuls du tableau, ainsi :

~~~~Bash
$ ./sieve 25
2 3 5 7 11 13 17 19 23
$
~~~~

## Tableaux de char (i.e. chaînes de caractères)

1. Écrire une fonction qui prend une chaîne de caractères en arguments
et renvoie la liste des sous-chaînes qui sont constitués de chiffres
(en base 10) consécutifs. (par exemple : avec "le nombre 42 commence à être
casse pied, pourquoi pas 43 ou 54 ?" on récupère le tableau de chaîne
contenant "42", "43" et "54".

_Note_ : vous pourrez vous inspirer de la solution de l'exercice 7.

2. Modifiez la fonction pour renvoyer une liste de nombres (int ou long int)

Le programme final fonctionnera ainsi :

~~~~Bash
$ ./extractnb "le nombre 42 commence à être casse pied, pourquoi pas 43 ou 54 ?"
42 43 54
$
~~~~

## Graphe d'une fonction réelle

Écrire un programme qui affiche sur un terminal de 80 caractères (au moins de large)
et 24 (au moins) de haut le graphe d'une fonction réelle sur un certain intervalle.

Indices :

- Écrire une fonction `graph(float x1, float x2, float y1, float y2)` qui rempli
un tableau d'entier 80x24 à deux dimensions en y mettant 0 pour pixel éteint et
1 pour pixel allumé comme représentation du graphe d'une fonction définie ailleurs
dans le programme : `f(float x)` par exemple

~~~~C
#include<math.h>
...

#define WIDTH  80
#define HEIGHT 24

int the_graph[WIDTH][HEIGHT];

float f(float x) {
    return x*x + 1;
}

void f_graph(float x1, float x2, float y1, float y2) {
...
}

int main() {
	f_graph(-4, 4, -1, 16);

	show_graph();
	
	exit(EXIT_SUCCESS);
}

~~~~

- Écrire une fonction qui affiche ce tableau sur le terminal en mettant le
caractère '*' pour représenter la courbe représentative et espace sur le
reste du plan.
- Vous pouvez aussi représenter les axes des abscisses et ordonnées avec,
repectivement, les caractères `-` et `|`  pour y.

## Listes chaînées

- Compléter la solution du TP12 sur les listes chaînées pour ajouter une
fonction qui ajoute une entrée à la position n (entier) dans une liste
chaînée. La fonction renverra 1 si l'insertion a réussi et 0 si elle
a échoué (liste trop courte pour la valeur de n, par exemple).

- Créez une variante de la la solution du TP12 pour stocker non pas
de nombre dans une liste chaînées mais des chaînes de caractères sous
forme de pointeur vers char

- Combinez cette solution avec la solution `split_rb` du TP7 pour
renvoyer une liste chaînées des mots extraits d'une chaîne
- Bonus : structurez la construction de ce projet avec deux bibliothèques
statiques, l'une déjà faite dans le TP8 (`libsplit...`) et l'autre
`libcharlist...`

## Listes doublement chaînées

- Partez du PT12 pour constuire des fonctions manipulant des listes
doublement chaînée, la structure `node` ayant alors la déclaration
suivante :

~~~~C
typedef struct node node;
struct node {
	int val;
        node *next;
        node *prev;
};
~~~~

Le champs `prev` étant alors un pointeur vers l'élément précédent
dans la liste (NULL pour le premier élément)


