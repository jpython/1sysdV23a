.PHONY: all clean

PROGS=cesar 01_vars 02_asknb 03_tests 04_pow 05_tableaux 06_chars 07_readchar 08_readstr \
      09_tabptr 11_intlist 13_quizz 10_struct 14_fork 
CC=gcc
CFLAGS=
LDFLAGS=-lm

all: $(PROGS)

%: %.c
	$(CC) $(CFLAGS) -o $@ $<  $(LDFLAGS)

%.s: %.c
	$(CC) -S $< 

clean:
	rm -f $(PROGS) *.s
