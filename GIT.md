# Introduction à GIT

Le plus tôt vous maîtriserez git mieux ce sera. Nous allons en voir
l'essentiel (nous ne parlerons pas des branches, des conflits, etc.)

## Systèmes de gestion de version

https://fr.wikipedia.org/wiki/Gestion_de_versions

https://en.wikipedia.org/wiki/Version_control

Permet de préserver l'historique des changements d'un ensemble
de fichiers, de travailler à plusieurs sur un même projet, de
créer des _branches_ (variantes) comme production, développement,
bugfixes, etc.

Apparus dans les années 70s sous UNIX (AFAIK) : SCCS, RCS, CVS, Subversion
La plupart sont dit "centralisés" : il faut un référentiel central, souvent
accessible par le réseau.

Insatisfait des systèmes existants, Linus Torvalds (le créateur du noyau
Linux !) décide de coder son propre system : GIT ("idiot") qui est
décentralisé à la base mais peut s'appuyer sur un référentiel extérieur
(via https ou ssh). 

## Comment créer un "dépot" GIT 

### Localement

~~~~Bash
$ git config --global --edit
~~~~

Modifiez pour mettre votre prénom et votre mail et 
vous sauvegardez et quittez.

~~~~Bash
$ mkdir myproject
$ cd myproject
$ git init .
$ echo Bonjour > myfile
$ echo Au revoir > otherfile
$ git add myfile otherfile
~~~~

Les deux fichiers myfile et otherfile sont maintenant _suivis_ ("traqués")

On enregistre leur état actuel en faisant un "commit" 

~~~~Bash
$ git commit -a -m 'starting project'
~~~~

`-a` signifie enregistrer l'état de tous les fichiers suivis
et `-m` pour ajouter un commentaire sur cet état.

On peut modifier ces fichiers :

~~~~Bash
$ nano myfile
$ nano otherfile
$ git commit -a -m 'first draft'
$ git status
On branch master
nothing to commit, working tree clean
jpierre@montpellier:~/myproject$ git log
commit 61ad39f74bb12f72f61ae7b6772304a04c40bc8d (HEAD -> master)
Author: Jean-Pierre <jp@xiasma.fr>
Date:   Tue Jun 27 12:16:53 2023 +0000

    first draft

commit 115d3ab6c6ab1a2c4985ea4c32f5f7205b730018
Author: Jean-Pierre <jp@xiasma.fr>
Date:   Tue Jun 27 12:16:16 2023 +0000

    init project

$ git diff 115d3ab 61ad39f
diff --git a/myfile b/myfile
index 632e4fe..169fedd 100644
--- a/myfile
+++ b/myfile
@@ -1 +1,2 @@
 Bonjour
+Comment ça va ?
diff --git a/otherfile b/otherfile
index 44f2557..e38ded6 100644
--- a/otherfile
+++ b/otherfile
@@ -1 +1,2 @@
 Au revoir
+Bon il fait chaud...
~~~~ 

Et on peut revenir en arrière, et faire plein de trucs sans craindre de perdre quelque chose...

Note : si vous supprimez, renommez ou déplacez des fichiers suivis il faut le faire à travers
git : `git rm fichier`, `git mv ...`.

On pourrait créer un dépôt git vide sur gitlab, framagit ou github et y "pousser" notre historique
mais il y a une autre façon, ci-dessous pour un nouveau projet :

### Sur une plate forme centralisé et import en local

1. Vous allez sur gitlab.com, identifiez-vous
2. Allez dans votre profil et ajoutez votre clef ssh
   publique `~/.ssh/id_rsa.pub` 
3. Créez un projet/dépôt nommé `1sysd`
   (vide, sans README, *public*)
4. Copiez son url de clonage `git@....` (PAS https)
5. Dans votre terminal clonez le en local : `git clone git@...` 

Exercice : Copiez le fichier hello.c dans ce répertoire (1sysd), faite en
sorte qu'il soit suivi, enregistrer un premier commit, et faites `git push` 

Sur l'interface Wed de gitlab vérifiez que ce fichier est bien visible.

Apportez quelques modifications à `hello.c`, commitez-les et examinez l'historique
sur l'interface web. 

Postez l'url de clonage https de votre dépôt dans le canal du cours : j'en ai 
besoin pour cloner vos dépôts et y trouver vos réponse aux questions de
l'examen final (4 heures, codage) ; il y aura aussi un QCM.

_Note_ : vous pouvez cloner aussi ce dépôt sur un Mac (brew install git) et sous
Windows (ssh est déjà installé, git est disponible là : https://gitforwindows.org/)

De même sur n'importe quel Linux vous pouvez le cloner et travailler en local
(insérez votre clef ssh publique du système concerné dans votre profil gitlab) :

~~~Bash
$ sudo apt install build-essential git
$ git clone git@.../1sysd
$ cd 1sysd
$ git status
~~~~ 

Git status montre que le fichier `a.out` (si il existe : `cc hello.c`) n'est pas
suivi. C'est *normal* étant un produit d'autres fichiers il ne doit pas être suivi
(et n'intéresse personne, c'est un binaire x86_64 pour GNU/Linux). Vous pouvez
créer un fichier `.gitignore` (qui lui doit être suivi) contenant une ligne :
`a.out`. `git status` ne s'en plaindra alors plus.

## Clonez mon dépôt, créé spécialement pour votre classe 

~~~Bash
$ cd
$ pwd
/home/jpierre
$ git clone https://framagit.org/jpython/1sysdV23a.git
$ cd 1sysdV23a
$ ls
~~~~

Après chaque commit/push de mon côté vous pourrez être
à jour en faisant `git pull` (si ce répertoire 1sysdV23a
est votre répertoire de travail).

