#include<stdlib.h>
#include<stdio.h>

int palindrome(char *s) {
    char *start, *end;

    // on liquide le cas de la chaîne vide
    if (*s == '\0') { // ou == 0
        return -1; // on dit que c'est un palindrome (mouais...)
    }
    // bon c'est pas vide
    start = end = s;
    while (*end) { // ou != '\0' ou != 0
        end++;
    } // ou while (*++end); avec bloc vide !
    // Attention : ce n'est pas le bloc du while
    end--; // on recule d'un élément : on est maintenant
           // sur le dernier 
    // On commence ainsi :
    // radar        serres      lapin
    // ^   ^        ^    ^      ^   ^
    // s   e        s    e      s   e
    // à la fin :
    // radar
    //   ^(s et e)    ^^          stop (pas égaux)
    //                se
    //   (égaux)      e = s+1     pas égaux
    // tant que *s == *e (même caractère) on avance s et recule e
    // cas 1 ou cas 2 : OK / sinon PAS OK
    while ( start < end && *start == *end ) {
	printf("Comparaison de %c et %c\n", *start, *end);
        start++;
        end--;
    }
    printf("start >= end : %d si non, comparaison de %c et %c\n", start < end, *start, *end);
    return *start == *end; 
}

int main() {
    char saisie[50];

    printf("Un mot : ");
    scanf("%s", saisie);
    if (palindrome(saisie)) {
        printf("C'est un palindrome.\n");
    } else {
        printf("Ce n'est pas un palindrome.\n");
    }
}
