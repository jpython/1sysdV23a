#include<stdio.h>
#include<stdlib.h>
#include<string.h>

typedef struct City City;
struct City {
    char *name; // ou pourrait envisager char name[50]
    int pop;
    float area;
    City *next;
};

void print_city(City *city) {
    printf("Ville de %s\n", city->name);
    printf("  Population : %d habitants\n", city->pop); 
    printf("  Surface : %.2f km²\n", city->area); 
}

City *read_city() {
    City *city;

    city = malloc(sizeof(City));

    printf("Nom de la ville (pas d'espace dans les noms) : ");
    scanf("%ms", &(city->name));
    printf("Population : ");
    scanf("%d", &(city->pop));
    printf("Surface : ");
    scanf("%f", &(city->area));
    city->next = NULL;

    return city;
}

void print_all_cities(City *head) {
    City *current;

    current = head;
    while( current ) {
        print_city(current);
        current = current->next; 
    }
}

City *new_city(char *name, int pop, float area) {
    City *city;
    city = malloc(sizeof(City));
    city->name = name;
    city->pop = pop;
    city->area = area;
    city->next = NULL;
}

City *insert_city(City *head, char *name, int pop, float area) {
    City *current;

    current = head;

    // deux cas  possible : la liste est vide (head est NULL)
    // ou pas...
    if ( head == NULL ) {
        head = new_city(name, pop, area);
    } else { // la liste n'est pas vide
        current = head;
        while ( current->next ) {
            current = current->next;
        }
        // arrivé ici on est sur un noeud qui n'a pas de successeur
        current->next = new_city(name, pop, area);
    }

    return head;
}
        
City *insert_city_node(City *head, City *new) {
    City *current;

    current = head;

    // deux cas  possible : la liste est vide (head est NULL)
    // ou pas...
    if ( head == NULL ) {
        head = new;
    } else { // la liste n'est pas vide
        current = head;
        while ( current->next ) {
            current = current->next;
        }
        // arrivé ici on est sur un noeud qui n'a pas de successeur
        current->next = new;
    }

    return head;
}

void print_densities(City *head) {
    City *current;
    float density;

    current = head;
    while( current ) {
        density = current->pop / current->area;
        printf("%s : %.2f hab/km2\n", current->name, density); 
        current = current->next;
    }
}

int total_pop(City *head) {
    City *current;
    int total = 0;

    current = head;
    while( current ) {
        total += current->pop;
        current = current->next;
    }
    return total;
}

City *find_by_name(City *head, char name[]) {
    City *current;
    int found = 0;

    current = head;
    while( current && !found ) {
        if (!strcmp(current->name, name)) {
            // on pourrait faire : break;
            found = 1; 
        } else {
            current = current->next;
        }
    }
    return current; 
}

City *remove_from_list(City *head, City *city) {
    City *current, *newhead;

    // liste vide : rien à faire
    // on pourrait ne pas tester et supposer que la liste n'est pas
    // vide...
    if (head == NULL) {
        return head;
    }
    // cas particulier : c'est la première ville...
    // Note : on peut éviter ça, et pour l'insertion aussi
    // j'ajouterai city_best.c d'ici demain dans le répertoire
    if (head == city) {
        newhead = head->next;
        free(head); // on libère la mémoire
        return newhead ; // évite un else
    }
    // sinon...
    current = head;
    while (current->next) {
        if (current->next == city) { // la suivante est la bonne
            current->next = current->next->next; // on "saute" l'elt à enlever
            free(city); // on libère la mémoire
            break;
        }
        current = current->next;
    }
    return head;
}

int length(City *head) {
    City *current;
    int len = 0;

    current = head;
    while( current ) {
        len++;
        current = current->next;
    }
    return len; 
}

int main() {
    int N;
    City *head = NULL;
    City *city;
    char *name;

    // head est mis à jour seulement lors du premier appel
    head = insert_city(head, "Versailles", 83583, 26.48);
    head = insert_city(head, "Montpellier", 299006, 56.88);
    head = insert_city(head, "Morlaix", 14709, 24.82);
    head = insert_city(head, "Édimbourg", 488050, 259.0);

    // head = insert_city_node(head, read_city());

    print_all_cities(head); 
    printf("Longueur de la liste %d.\n", length(head));
    print_densities(head); 
    printf("Population totale : %d habitants.\n", total_pop(head));

    printf("Nom de ville à trouver : ");
    scanf("%ms", &name); 
    if (city = find_by_name(head, name)) { // = et pas ==
        printf("Trouvée !\n");
        print_city(city);
        head = remove_from_list(head, city);
        printf("\n\nVoici la liste une fois la ville enlevée :\n");
        print_all_cities(head);
    } else {
        printf("Ville inconnue.\n");
    }
    print_all_cities(head);
    printf("Longueur de la liste %d.\n", length(head));
    // test suppression si liste vide
    // ne doit pas crasher
    head = NULL;
    head = remove_from_list(head, (City *)42);
    exit(0);
}

