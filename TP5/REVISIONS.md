# Révisions sur tableaux et chaînes


## Palindromes...

Écrire une fonction qui renvoie 1 si la chaîne passée en argument est un
plaindrome (comme "radar" ou "abba") et 0 sinon.

- On peut traiter le problème
  avec des indices numériques _i_ et _j_ qui partent du premier et dernier
 caractère de la chaîne et se rapporchent l'un de l'autre.
- On peut y arriver aussi avec deux pointeurs _p_ et _q_ positionnés sur
  le premier et le dernier caractère de la chaîne et jouer sur l'arithmétique
  de pointeurs

## Tableaux de nombres

Écrire un programme qui initialise une liste de nombres entiers d'une
taille quelconque mais raisonnable (10 par exemples).

Écrire une fonction `print_numbers(int tab[], int N)` qui affiche les
élément du tableau sur une ligne en séparant les valeur par une espace.

Implémenter l'algorithme de _tri par insertion_ pour modifier ce tableau
afin qu'il soit trié dans l'ordre croissant dans une fonction `sort_numbers(int tab[])`,
testez en affichant le résultat.

