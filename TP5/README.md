# Pointeurs vers structures

## Syntaxe 

~~~~C

City *p;

...

printf("%s", p->name); // plus lisible (*p).name
~~~~

## À quoi ça peut bien servir ?

Imaginons qu'on souhaite stocké une liste arbritrairement longue
de villes... Allouer un tableau de plusieurs centaines de milliers
d'éléments n'est pas raisonnable...

On souhaite travailler dynamiquement. Par exemple si j'ajoute une ville
à mon jeu de données la mémoire est allouée à ce moment là.

Un représentation possible est une liste chaînée : chaque éléments
va avoir un champ en plus, typiquement nommé `next` (ou `suivant`)

~~~~C
typedef struct City city;
struc City {
    char name[50];
    int pop;
    float area;
    City *next; // pointeur vers la ville suivante
                // NULL si c'est la dernière
}
~~~~

~~~~
        | name: Paris         ----> | name: Montpellier       ---->| name: Versailles
head -> | pop: 20000000      /      | pop: 800000            /     | pop: 500000
        | area: 71.23       /       | area: 100.12          /      | area: 20.61
        | next: -----------/        | next: ---------------/       | next : NULL
~~~~

Pour créer une ville :

~~~~C

City *read_city() {
    City *city;

    city = malloc(sizeof(City));
    ... saisie des données
    city->next = NULL;
    return city;
}

~~~~

## Parcours d'une liste chaînée 

Prenez le temps de lire et de bien comprendre le code de départ. Copiez
le fichier `city_start.c` sous le nom `city.c` (`cp city_start.c city.c`)
et ajoutez y deux fonctions :

- L'une qui affiche les noms de toutes les villes suivi de leur densité de population
- L'autre qui calcule la population totale de toutes les villes d'une liste chaînée

Testez vos fonctions.

## Recherche et suppression

Ajoutez à votre code une fonction qui renvoie un pointeur vers l'élément
de la liste qui contient les informations sur une ville ayant un certain
nom (ou NULL si absent de la liste) `City *find_by_name(City *head, char name[])`, testez
votre fonction (dans les deux cas : ville présente ou absente).

Note : vous pouvez utiliser la fonction `strcmp` (`man strcmp`) pour comparer
deux chaînes (ou la fonction `sequal` d'un TP précédent).

Ajoutez une fonction qui supprime un élément de la liste, que l'on pourrait
utiliser ainsi : 

~~~~C
print_all_cities(head);
city = find_by_name(head, "Montpellier");
if (city) { // comme != NULL
    head = remove_from_list(head, city);
}
print_all_cities(head);
~~~~

Pensez à appeler la fonction `free()` pour libérer la mémoire occupée par
l'élément supprimé.

Pour finir : écrire une fonction qui renvoie la longueur d'une liste chaînée de ville,
testez là.

