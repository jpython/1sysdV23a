## Le C en 20 heures

Eric Berthomier, Daniel Schang

https://archives.framabook.org/le-c-en-20-heures-2/index.html

## Unix. Pour aller plus loin avec la ligne de commande.

_(Tout ce que vous avez toujours voulu savoir sur Unix sans jamais oser le demander)_

Vincent Lozano

https://archives.framabook.org/unixpou-allerplusloinaveclalignedecommande/index.html

## Programmation en langage C

Anne CANTEAUT

https://www.rocq.inria.fr/secret/Anne.Canteaut/COURS_C/cours.pdf
