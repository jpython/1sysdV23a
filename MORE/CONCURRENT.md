# Modèle de calculs parallèles

## UNIX : fork/exec

Un processus peut demander sa duplication (comme une bactérie) avec
la fonction `fork()`. La fonction renvoie 0 si on est le processus
"enfant" et le numéro du processus "enfant" si on est le "parent".

Voir la commande UNIX : `pstree` 

La "duplication" est très rapide : lors de leurs "vies" futures le
noyau va simplement dupliquer les zones mémoires qui changent entre
les deux processus (COW : _Copy On Write_).

Ensuite le processus "enfant" peut demandre à `exec`-uter un autre
binaire : c'est que fait un Shell quand vous tapez une commande
externe.

## Les threads _i.e._ "processus légers"

On crée une nouvelle tache avec une fonction de la bibliothèque 
'libpthread' qui prend une fonction en argument : c'est le code
qui sera exécuté.

La mémoire est partagée entre les fonctions : pb de synchronisation
pour éviter les conflits...

MS Windows implémente quelque chose de similaire.

Note : Le noyau Linux implémente les deux modèles avec un appels
système unique nommé `clone` qui est "caché" derrière la fonction
`fork` et la bibliothèque libpthread.
