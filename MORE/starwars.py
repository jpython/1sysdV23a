#!/usr/bin/env python3

import os
from time import sleep

val = 42

if pid := os.fork():
    # pid > 0 : nous sommes le "parent"
    print("Dark Vador : Luke je suis ton père !")
    sleep(1)
    print("Dark Vador : Je suis le processus numéro", os.getpid())
    print("Dark Vador : tu es le processus numéro", pid)
    val += 1
    print("Dark Vador : la valeur est :", val)
else:
    # pid = 0 : nous sommes l'"enfant"
    print("Luke : ah ?")
    val -= 1
    print("Luke : la valeur est :", val)

sleep(10)
print("Je suis le processus numéro", os.getpid(), f"bonjour chez vous ! {val=}") 
