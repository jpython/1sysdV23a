#include<stdio.h>
#include<stdlib.h>
#include<time.h>

int main() {
    int n = 0, secret, guess;

    srand(time(NULL)); 
    printf("Essayez de deviner un nombre entre 1 et 100 !\n");
    secret = rand() % 100 + 1;
    // do {...} while(...) : entre au moins une fois dans la boucle
    // indépendamment du résultat du test
    // (en Pascal : REPEAT ... UNTIL cond
    // un peu bizarre : mathématiquement ça tient pas debout
    do {
        printf("Votre idée : ");
	scanf("%d", &guess);
	n++;
	if (guess == secret) {
		break;
	}
	if (guess > secret) {
	     printf("Trop grand...\n");
	} else {
	     printf("Trop petit...\n");
	}
    } while(1);

    printf("Gagné ! En %d coups !\n", n);
    printf("Bravo !\n");
    return 0;
} 
