#!/usr/bin/env python3

from random import randint

nmin,nmax = 1,100

notfound = True

guess = (nmin + nmax)//2

n = 0

while (notfound):
    n += 1
    print(f"Est-ce {guess} ?")
    ans = input("+ (si trop petit), - (si trop grand) ou = : ");
    if ans == '+':
        nmin = guess
    elif ans == '-':
        nmax = guess
    else:
        notfound = False
    guess = (nmin + nmax)//2

print("\N{Smiling Face with Smiling Eyes} 🥳 🎉 "
              f"Ouais !!! J'ai gaaagné en {n} coups !!!")

