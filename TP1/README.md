# TP1 : Des nombres et des jeux

## Nombres aléatoires

Examinez le code des fichiers TP0.1, concernant les nombres 
pseudo-aléatoires. Il existe une fonction srand() qui renvoie
un nombre aléatoire (man srand) : on constate que c'est toujours
les mêmes.... Ben oui : un ordinateur est une machine déterministe.

Comment faire ? On peut spécifier une « graine » qui est la valeur
de départ de la suite récurrente utilisée par la "fonction", un
point de départ convenable est le "temps" courant (l'epoch : le
nb de secondes depuis le 1/1/1970 à 00:00)

~~~~Bash
$ date +"%s"
1688109110
$ date +"%s"
1688109140
~~~~

Ça garantit un "hasard" raisonnable (ok pour des jeux, mais pas assez
pour la cryptographie).

https://fr.wikipedia.org/wiki/Pseudo-al%C3%A9atoire

https://fr.wikipedia.org/wiki/G%C3%A9n%C3%A9rateur_de_nombres_pseudo-al%C3%A9atoires

Du "vrai" aléatoire :

https://en.wikipedia.org/wiki/Lavarand

(Il existe des composants électroniques contenant un matériau radioactif
dans certains ordinateurs pour générer du hasard !)

## Un jeu vidéo !

On se propose d'écrire un programme en C qui implémente un classique :
deviner un nombre choisi au "hasard" par l'ordinateur qui nous indique
si notre proposition est plus grande ou plus petite :

~~~~Bash
$ ./game 
Essayez de deviner un nombre entre 1 et 100 !
Votre idée : 50
Trop petit...
Votre idée : 75
Trop grand...
Votre idée : 60
Trop grand...
Votre idée : 55
Trop grand...
Votre idée : 54
Gagné ! En 5 coups !
Bravo !
~~~~

## Le jeu dans l'autre sens

Écrire un programme (qu'on peut prototyper en Python) qui tente de deviner
un nombre entre 1 et 100 que l'utilisateur a choisi. À chaque étape 
le programme propose un nombre et l'utilisateur répond + si le nombre
qu'il a choisi est plus grand, - s'il est plus petit et = si le programme
l'a trouvé.

Une fois bien testé, on peut ajouter du code permettant au programme de
détecter une tricherie de la part de l'utilisateur qui interrompt le
programme après affichage d'une protestation !

Implémenter le même programme en C.

Indice : pour demander un caractère à l'utilisateur voir le source
`07_readchar.c`.

Solution :

Comme dans notre tête lorsque nous jouons, on actualise l'information :
« le nombre à deviner est entre XXX et YYY », le "cerveau" de l'I.A.
est constitué de deux nombres entier nmin et nmax.

Au début tout ce que l'on sait est que nmin = 1 et nmax = 100.

Au fur et à mesure on actualise nmin (qui augmente) si le nombre à
deviner est plus grand et nmax (qui diminue) si le nombre à deviner
est plus petit ; dans les deux cas la nouvelle valeur est notre proposition.

La proposition peut être un nombre au hasard entre nmin + 1 et nmax si
c'est nmin qui change et nmin et nmax - 1 si c'est nmax qui change.

La stratégie optimale (si le tirage aléatoire est uniforme) est de prendre
la division euclidienne de (nmin + nmax) par 2.

revgame.py et revgame2.py implémentent ces deux stratégies en Python 3.

Note : bien pensez à faire beaucoup de tests pour vérifier que la stratégie
du programme est bien _gagnante_ et qu'il ne fait pas de sottises comme
proposer deux fois le même nombre (si le choix du nombre proposé est
aléatoire), testez en particulier les cas où le programme a raison tout
de suite ou si l'utilisateur "choisit" 1 ou 100.

Reste la question de la détection de la triche.

Si nmin dépasse nmax on est sûr de la triche, si ils sont égaux aussi,
et si ils sont l'un après l'autre après qu'on ait rejeté la réponse.
À méditer mathématiquement...


