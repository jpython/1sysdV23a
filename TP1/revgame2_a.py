#!/usr/bin/env python3

from random import randint

nmin,nmax = 1,100

guess = (nmin + nmax)//2

n = 0

while True:
    n += 1
    print(f"{nmin=} {nmax=}")
    print(f"Est-ce {guess} ?")
    ans = input("+ (si trop petit), - (si trop grand) ou = : ");
    if ans == '+':
        nmin = guess
    elif ans == '-':
        nmax = guess
    else:
        break
    # à réfléchir, ça a l'air d'être ça, mais il faut
    # faire des tests et/ou des maths pour être
    if nmax - nmin <= 1:
        print("😠 Vous trichez !!! Je ne joue plus.")
        exit(1)
    guess = (nmin + nmax)//2

print("\N{Smiling Face with Smiling Eyes} 🥳 🎉 "
              f"Ouais !!! J'ai gaaagné en {n} coups !!!")

