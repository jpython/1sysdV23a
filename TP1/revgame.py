#!/usr/bin/env python3

from random import randint

nmin,nmax = 1,100

notfound = True

guess = randint(nmin, nmax)

n = 0 

while (notfound):
    n += 1
    print(f"Est-ce {guess} ?")
    ans = input("+ (si trop petit), - (si trop grand) ou = : ");
    if ans == '+':
        nmin = guess
        guess = randint(nmin + 1, nmax)
    elif ans == '-':
        nmax = guess
        guess = randint(nmin, nmax - 1)
    else:
        notfound = False

print("\N{Smiling Face with Smiling Eyes} 🥳 🎉 "
      f"Ouais !!! J'ai gaaagné en {n} coups !!!")
